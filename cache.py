from robot import Robot
class Cache:
    def __init__(self):
        self.cache = {} #diccionario para guardar los documentos web descargados

    def retreive(self, url):
        if url not in self.cache:
            self.cache[url] = Robot(url)

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for url in self.cache:
            print('Las urls de la cache son:', url)
            
    def content(self, url):
        self.retreive(url)
        return self.cache[url].content()


