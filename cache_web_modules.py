from cache import Cache

if __name__ == "__main__":
    cache_url = Cache()
    cache_url.retreive('https://www.google.com')
    cache_url.retreive('https://www.urjc.es/')
    cache_url.show('https://gsyc.urjc.es/')
    cache_url.show_all()