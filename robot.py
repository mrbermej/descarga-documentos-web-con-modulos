"""Clase Robot. Proporcionará un robot que se encarga de descargar, y en su
caso mostrar, un documento web, dada su url. Cumplirá la siguiente especificación:
 Se instanciará indicando como argumento la url del documento del que
se ocupará el robot.
 Tendrá un método retrieve, sin argumentos, que se encargará de des-
cargar el documento de la url de la que se ocupa el robot, sólo si no se
lo ha descargado ya antes. Si se lo descarga, mostrará un mensaje en
pantalla: Descargando url (siendo "url" la url en cuestión).
 Tendra un método show, sin argumentos, que mostrará en pantalla el
contenido del documento descargado. Para poder mostrarlo, show se
encargará de usar retrieve cuando sea conveniente.
 Tendrá un método content, sin argumentos, que devolverá una cadena
de caracteres (string) con el contenido del documento descargado."""
import urllib.request
class Robot:
    def __init__(self, url):
        self.url = url
        self.recibido = False #indica que el contenido de la URL se ha descargado.

    def retrieve(self):
        # si el contendio de la URL no se ha descargado entonces lo abrimos, lo decodificamos y lo guardamos en self.content
        if not self.recibido:
            print("Descargando url: ", self.url)
            url = urllib.request.urlopen(self.url)
            self.content= url.read().decode('utf-8')
            self.recibido = True
            return self.content

    def show(self):
        print(self.content) #printeamos el contenido de la url decodificado

    def content(self):
        self.retrieve()
        return self.content #esto se mete en la funcion retreive y devuelve el contenido, pero no hace más